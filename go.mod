module gitlab.com/idawson/youcallme

go 1.16

require (
	github.com/creack/pty v1.1.17
	golang.org/x/net v0.0.0-20211020060615-d418f374d309
)
